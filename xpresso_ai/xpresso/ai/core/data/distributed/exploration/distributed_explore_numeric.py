"""Exploration on Numeric attribute column"""

__all__ = ["DistributedExploreNumeric"]
__author__ = ["Sanyog Vyawahare"]

from xpresso.ai.core.commons.utils.constants import DEFAULT_OUTLIER_MARGIN_NUMERIC, \
    DEFAULT_PROBABILITY_BINS, \
    DEFAULT_ATTRIBUTE_VALIDITY_THRESHOLD
from xpresso.ai.core.data.automl.dataset_type import DECIMAL_PRECISION


class DistributedExploreNumeric():
    """Class for numeric analysis"""

    def __init__(self, data, threshold=DEFAULT_ATTRIBUTE_VALIDITY_THRESHOLD,
                 outlier_margin=DEFAULT_OUTLIER_MARGIN_NUMERIC,
                 probability_dist_bins=DEFAULT_PROBABILITY_BINS):
        self.data = data
        self.outlier_margin = outlier_margin
        self.probability_dist_bins = probability_dist_bins
        if threshold is None:
            self.threshold = DEFAULT_ATTRIBUTE_VALIDITY_THRESHOLD
        else:
            self.threshold = threshold

    def populate_numeric(self):
        """Sets the metrics for numeric attribute
            Args:
            Return:
                """
        resp = dict()
        minimum, maximum, mean, median, std, var, mode, quartiles, \
        outliers, iqr, kurtosis, skewness, is_valid = self.numeric_analysis(
            self.data, self.threshold, self.outlier_margin)

        resp["min"] = minimum
        resp["max"] = maximum
        resp["mean"] = mean
        resp["median"] = median
        resp["std"] = std
        resp["var"] = var
        resp["mode"] = mode
        resp["quartiles"] = quartiles
        resp["outliers"] = outliers
        resp["iqr"] = iqr
        resp["kurtosis"] = kurtosis
        resp["skewness"] = skewness
        resp["is_valid"] = is_valid
        return resp

    @staticmethod
    def numeric_analysis(data, threshold, outlier_margin=DEFAULT_OUTLIER_MARGIN_NUMERIC):
        """Performs analysis of numeric attribute
        Args:
        Returns:
                minimum (:int): Minimum numeric value
                maximum (:int): Maximum numeric value
                mean (:int): Mean of the values of attribute
                median (:int): Median of the values of attribute
                std (:int): Standard Deviation of the values of attribute
                var (:int): Variance of the values of attribute
                mode (:int): Mode of the values of attribute
                quartiles (:list): Quartiles of attribute
                deciles (:list): Deciles of attribute
               outliers (:list): Outliers of the attribute
               pdf (:dict): Probability distribution of the values of attribute
               iqr (:list): Inter quartile range
               kurtosis (:int): Kurtosis of the values of attribute
               is_valid(:bool): Boolean indicating validity of attribute
           """
        summary = data.describe().round(DECIMAL_PRECISION)

        minimum = summary.at["min"].round(DECIMAL_PRECISION)
        maximum = round(data.max(), DECIMAL_PRECISION)
        mean = summary.at["mean"].round(DECIMAL_PRECISION)
        std = summary.at["std"].round(DECIMAL_PRECISION)
        var = (std * std).round(DECIMAL_PRECISION)
        mode = data.mode().round(DECIMAL_PRECISION)
        if len(mode) >= 1:
            mode = mode.min()
        else:
            mode = "na"
            print("Unable to find mode for {}.".format(data.name))
        quartiles = data.quantile([.0, .25, .5, .75, 1.0]).round(
            DECIMAL_PRECISION)
        quartiles = quartiles.to_list()
        if len(quartiles) == 5:
            median = quartiles[2]
            iqr = quartiles[3] - quartiles[1]
        else:
            iqr = "na"
            median = "na"
            print("Insufficient number of quartiles, Unable to find inter "
                  "quartile range and median for {}.".format(data.name))

        outliers = data.loc[((data - mean).abs() > (outlier_margin * std))].round(
            DECIMAL_PRECISION)
        outliers = outliers.to_list()
        kurtosis = round(data.kurtosis(), DECIMAL_PRECISION)
        skewness = round(data.skew(axis=0, numeric_only=True), DECIMAL_PRECISION)
        total_data = data.size
        isnumeric_count = total_data - data.isna().sum()
        numeric_percent = round(float(isnumeric_count / total_data) * 100
                                , DECIMAL_PRECISION)
        if numeric_percent >= threshold:
            is_valid = True
        else:
            is_valid = False

        return minimum, maximum, mean, median, std, var, mode, quartiles, \
               outliers, iqr, kurtosis, skewness, is_valid
